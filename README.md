# Die Bass - SpectrumAnalyser
Spectrum Analyser running on Teensy 3.2 and transfering data over i2c to a display adapter eg. an Arduino with WS2812B LEDs
## Build Instructions
To build the code please change the following your libraries:

In the file `%ARDUINO_INSTALL_DIR%/hardware/teensy/avr/cores/teensy3/AudioStream.h` change the following lines:

```
#ifndef AUDIO_SAMPLE_RATE_EXACT
#if defined(__MK20DX128__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
#define AUDIO_SAMPLE_RATE_EXACT 44117.64706 // 48 MHz / 1088, or 96 MHz * 2 / 17 / 256
#elif defined(__MKL26Z64__)
#define AUDIO_SAMPLE_RATE_EXACT 22058.82353 // 48 MHz / 2176, or 96 MHz * 1 / 17 / 256
#endif
#endif
```

into

```
#ifndef AUDIO_SAMPLE_RATE_EXACT
#if defined(__MK20DX128__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
//#define AUDIO_SAMPLE_RATE_EXACT 44117.64706 // 48 MHz / 1088, or 96 MHz * 2 / 17 / 256
#define AUDIO_SAMPLE_RATE_EXACT 11029.41176
#elif defined(__MKL26Z64__)
#define AUDIO_SAMPLE_RATE_EXACT 22058.82353 // 48 MHz / 2176, or 96 MHz * 1 / 17 / 256
#endif
#endif
```

In the file `%ARDUINO_INSTALL_DIR%/hardware/teensy/avr/libraries/Audio/utility/pdb.h` change the following lines:

```
#elif F_BUS == 36000000
  #define PDB_PERIOD (816-1)
elif F_BUS == 24000000
```

into

```
#elif F_BUS == 36000000
  //#define PDB_PERIOD (816-1)
  #define PDB_PERIOD (3264-1)
#elif F_BUS == 24000000
```
