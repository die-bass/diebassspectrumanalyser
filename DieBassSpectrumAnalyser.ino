
/* ********************************************************
 * IMPORTANT NOTICE: PLEASE RUN TEENSY AT 72MHZ SPEED !!! *
 **********************************************************/

#include <Audio.h>
#include <Wire.h>

//#define DEBUG


AudioInputAnalog         adc1(A1);        //xy=144,119
AudioAnalyzeFFT1024      fft_lib;       //xy=498,64
AudioConnection          patchCord1(adc1, fft_lib);

/* LED configuration */
const int led_row_number = 9;
const int led_col_number = 24;

/* gain of audio input - lower the number if the audio input is higher */
float input_gain = 50.0;
/* auto gain control - counter to count time since last overdrive of gain */
unsigned int input_gain_counter = 0;
/* auto gain control - threashold in frames until rising gain 2000 = 90 sec */
const unsigned int input_gain_rise_threashold = 2000;
unsigned int input_gain_rise_threashold_current = input_gain_rise_threashold;
/* value of silence eg. music paused */
const float fft_silence_val = 0.01;

/* data type for led state sent over i2c */
union band_states {
  uint8_t bands[24];
  byte data[24];
} leds;

/* ISR will be called of state request of i2c master - keep this function as small as possible */
void requestLedStates() {
  Wire.write(leds.data, 24);
}

void setup() {
  /* allocate memory for audio data from audio input */
  AudioMemory(12);
  
  #ifdef DEBUG
  delay(1000);
  Serial.begin(57600);
  delay(1000);
  /* output sample rate configuration */
  Serial.print("sample rate exact: ");
  Serial.println(AUDIO_SAMPLE_RATE_EXACT);
  Serial.print("sample rate: ");
  Serial.println(AUDIO_SAMPLE_RATE);
  Serial.println("f_cpu: ");
  Serial.println(F_CPU);
  Serial.println("f_bus: ");
  Serial.println(F_BUS);
  #endif

  /* start i2c bus - join as slave with adress 0 */
  Wire.begin(0);
  Wire.onRequest(requestLedStates);

  /* use Hanning window function after fft analysis */
  fft_lib.windowFunction(AudioWindowHanning1024);

  /* reset current led state */
  for (int i=0; i<led_col_number; i++) {
    leds.bands[i] = 0;
  }

  /* indicate setup done */
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
}

unsigned int calculateInput(float in) {
  /* use natural logrithm to normalise signal */
  return (unsigned int)(log((double)((in*2)+1)) * input_gain);
}

void loop() {
  unsigned int band_val;
  float fft_val;
  int band;
  bool input_gain_counter_check = false;

  if (fft_lib.available()) {
    #ifdef DEBUG
    Serial.print("FFT: ");
    #endif
    /* if input did not overdrive for the threashold of frames - rise input and lower threashold for quicker adjustment */
    if (input_gain_counter >= input_gain_rise_threashold_current) {
      input_gain = input_gain * 1.05;
    }
    /* iterate over all bands that fit on display starting from the lowest band */
    for (band=0; band<led_col_number; band++) {
      /* skip the first two bands as the frequencies are too low for normal music */
      fft_val = fft_lib.read(band+2);
      /* if no music is playing do not increase input_gain_counter */
      if (fft_val > fft_silence_val)
        input_gain_counter_check = true;
      band_val = calculateInput(fft_val);
      /* trim bar down to max size of display */
      if (band_val > led_row_number) {
        /* if band overdrives by two lower the gain for the next frame - allow overdrive of five because it gives the feeling that the bass is punching :) */
        if (band_val > led_row_number + 5) 
          input_gain = input_gain * 0.95;
        input_gain_counter = 0;
        input_gain_rise_threashold_current = input_gain_rise_threashold;
        band_val = led_row_number;
      }
      /* sanity check for positiv bar value only */
      if (band_val < 0) band_val = 0;
      leds.bands[band] = (unsigned char)band_val;
      #ifdef DEBUG
      if (fft_val >= fft_silence_val) {
        Serial.print("  ");
        Serial.print(fft_val);
        Serial.print("  ");
      } else Serial.print("  -  "); // don't print "0"
      #endif
    }
    /* rise input_gain_counter if there was music playing */
    if (input_gain_counter_check)
      input_gain_counter++;
    #ifdef DEBUG
    Serial.println();
    #endif
  }

  delay(30);
}
